'use strict';
const mysqlssh = require('mysql-ssh');
const fs = require('fs');
const async = require('async');
const request = require('request');
const moment = require('moment');
const slack   = require('slack-node');
const username = require('username');
const webHookUrl = 'https://hooks.slack.com/services/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';
let slackHook = new slack();
slackHook.setWebhook(webHookUrl);
module.exports = function(Godrejrequestdata) {

	Godrejrequestdata.TestCron = function(data, cb) {
		async.auto({
			getGodrejRequestData:function(callback){
				mysqlssh.connect({
						host: 'ssh.servify.tech',
						user: 'dbuser',
						privateKey: fs.readFileSync(process.env.HOME + '/.ssh/amolsalgare-GitHub')
					}, {
						host: '172.31.0.77',
						user: 'InProdWS_read',
						password: 'fyCXfehhq#tRa',
						database: 'servify_integration'
					})
					.then(client => {
						client.query("Select addtime(max(CreatedDate), '05:30') as CreationDate from godrej_request_data", function(err, results, fields) {
							if (err) throw err
							console.log('results --> ', results);
							mysqlssh.close()
							return callback(null, moment(results[0].CreationDate).utcOffset('+0530').format('YYYY-MM-DDTHH:mm:ss'));
						})
					})
					.catch(err => {
						return callback(err);
					})
			},
			sendAlert:['getGodrejRequestData',function(results,callback){
				console.log('results.getGodrejRequestData ',results.getGodrejRequestData);
				console.log('moment',moment().format('YYYY-MM-DDTHH:mm:ss'));
				//let gs= (results.getGodrejRequestData - moment().format('YYYY-MM-DDTHH:mm:ss'))/1000;
				let gs= moment().diff(results.getGodrejRequestData);
				
				let time = Math.round(gs/(1000*60));
				let slackText;
				console.log('time ',time);
				if (time >= 30) {
					slackText = '<!channel> No Entry Found From Last Half an hour';
				} else {
					slackText =username.sync() + ' - Last Entry Found at '+results.getGodrejRequestData;
				}
				
				slackHook.webhook({
					channel: "godrej-fst-monitoring",
					username: "godrej-fst-bot",
					text: slackText
				}, function(slackError, slackResponse) {
					if (slackResponse && slackResponse.status && slackResponse.status === 'fail') {
						console.log('Failed to fire slack hook: ', slackError);
						return callback()
					} else {
						//console.log('slackResponse ',slackResponse);
						return callback()
					}
				});
			}]
		},function(error, autoResult){
			if(error){
				return cb(null,{
					success:true,
					msg:'Something went wrong',
					data:error
				});
			}else{
				return cb(null,{
					success:true,
					msg:'Cron successfully run'
				});
			}
		});


	}

	Godrejrequestdata.remoteMethod(
		'TestCron', {
			description: 'onsiteCommunication',
			accepts: {
				arg: 'data',
				type: 'object',
				default: '{ "ConsumerServiceRequestID": 7 }',
				required: true,
				http: {
					source: 'body'
				}
			},
			returns: {
				arg: 'result',
				type: 'object',
				root: true,
				description: 'Modes Object'
			},
			http: {
				verb: 'post'
			}
		});

};
